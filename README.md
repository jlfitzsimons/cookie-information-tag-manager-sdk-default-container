# Google Tag Manager and Cookie Information Cookie Control SDK v1

## Introduction

The Cookie Information Cookie Control SDK via Google Tag Manager is a pre-defined container that you can then import into your own Google Tag Manager account and use to become compliant with GDPR.

## Why has this been done?

This has been done because we are aware that the Cookie Control SDK in its current form can be quite daunting and somewhat confusing for those who are not already familiar with Google Tag Manager and how it is configured. 

By providing a pre-defined container with common Tags that have the SDK already applied, a lot of the work has been removed from the process of ensuring that you do not set cookies (other than necessary) before your website visitors have given their consent.

## How does it work?

The Cookie Control SDK works by adding a blocking trigger (that relies on a custom variable) that can be added to Tags within your Tag Manager container. By adding these tags, you are telling Google Tag Manager that they are not allowed to fire until a certain condition is met e.g. do not allow the Facebook Pixel script to fire unless the user has consented to marketing cookies.

## Usage 

### Downloading the JSON file

Click Downloads on the left hand menu:

![Downloadjson1](https://bitbucket.org/jlfitzsimons/cookie-information-tag-manager-sdk-default-container/raw/c04a9a0dfb29b53de206306838d31120d0088196/READMEmedia/Sk%C3%A6rmbillede%202021-06-04%20kl.%2008.45.49.png)

Select the Branches tab:

![Downloadjson2](https://bitbucket.org/jlfitzsimons/cookie-information-tag-manager-sdk-default-container/raw/c04a9a0dfb29b53de206306838d31120d0088196/READMEmedia/Sk%C3%A6rmbillede%202021-06-04%20kl.%2008.46.48.png)

On the same line as the branch named "adding-json-file" click on the .zip option (this will then download it):

![Downloadjson3](https://bitbucket.org/jlfitzsimons/cookie-information-tag-manager-sdk-default-container/raw/c04a9a0dfb29b53de206306838d31120d0088196/READMEmedia/Sk%C3%A6rmbillede%202021-06-04%20kl.%2008.47.44.png)

When it has finished downloading, unzip the file (double click and extract it to a folder of your choosing).

### Adding the Cookie Information Default Container to your own Google Tag Manager account

If you have not already created a Google Tag Manager account, please do so by using the following link and signing in with your Google account details: [tagmanager.google.com](https://tagmanager.google.com/) 

In order to add this container to your Google Tag Manager account, you willl need to download the JSON file that is included in this repository. 

Once you have downloaded the JSON file, go to the Admin panel of your Google Tag Manager account:

![GTMAdmin1](https://bitbucket.org/jlfitzsimons/cookie-information-tag-manager-sdk-default-container/raw/7c6ef15b01e21b30dad6bcd627dd7fbff057d31e/READMEmedia/Sk%C3%A6rmbillede%202021-06-03%20kl.%2014.05.17.png)


On the right hand side of the page you will now see settings for your container. Please select the option "Import Container"

![GTMAdmin2](https://bitbucket.org/jlfitzsimons/cookie-information-tag-manager-sdk-default-container/raw/2273207f1b38fe2ba6efe7e093f37c2b1286b0be/READMEmedia/Sk%C3%A6rmbillede%202021-06-03%20kl.%2014.07.33.png)

Click "Choose container file" and select the JSON file that you downloaded earlier. Please also choose whether you would like to have this container imported **into your existing workspace** or **into a new workspace**. 

You can also select whether you would like to overwrite everything in your existing workspace, or merge the contents of the Default container into it instead.



[GIF goes here]

If you choose a new workspace, you'll be presented with the option to give it a name and add a description:


![AddToNewWorkspace](https://bitbucket.org/jlfitzsimons/cookie-information-tag-manager-sdk-default-container/raw/2273207f1b38fe2ba6efe7e093f37c2b1286b0be/READMEmedia/Sk%C3%A6rmbillede%202021-06-03%20kl.%2014.24.39.png)


If you decide that you're importing it into an existing workspace, you will be presented with a menu where you can select which workspace it should be added to:

![AddToExistingWorkspace](https://bitbucket.org/jlfitzsimons/cookie-information-tag-manager-sdk-default-container/raw/2273207f1b38fe2ba6efe7e093f37c2b1286b0be/READMEmedia/Sk%C3%A6rmbillede%202021-06-03%20kl.%2014.26.51.png)

Once you have picked, a little further down you should see the changes that are about to be made if you import the container (I have chose the existing workspace and said that it should be overwritten in the below example):

![Summaryofchanges](https://bitbucket.org/jlfitzsimons/cookie-information-tag-manager-sdk-default-container/raw/2273207f1b38fe2ba6efe7e093f37c2b1286b0be/READMEmedia/Sk%C3%A6rmbillede%202021-06-03%20kl.%2014.38.13.png)


![WorkspaceSummary](https://bitbucket.org/jlfitzsimons/cookie-information-tag-manager-sdk-default-container/raw/2273207f1b38fe2ba6efe7e093f37c2b1286b0be/READMEmedia/Sk%C3%A6rmbillede%202021-06-03%20kl.%2014.39.30.png)

Finally, add a description to your container.

## What changes do I need to make to the Default Tags

Because this is a base container with everything that you will need in place from the beginning, the only thing you have to do is:

1. **Ensure that your measurement IDs for different services are added to the tags**. Currently, all that the tags contain are the scripts for each service - if you do not add your specific ID for the service, then it will not know which account it should be tracking results to.
2. **Add any other conditions that a tag is allowed to fire on**. As an example, if you have a sign up form and use HubSpot to gather information about these sign ups, it would be a good idea to add an extra condition that the tag can only fire the page yourwebsite.com/sign-up

## What if I already have a Tag Manager Container set up with all my tags?

If you already have a Tag Manager Container set up with all your tags in it, then please choose the JSON file  that only contains the [Cookie Control SDK Triggers and Variables](https://bitbucket.org/jlfitzsimons/cookie-information-tag-manager-sdk-default-container/get/add-gtm-variables-triggers-only.zip).

Once you have imported these (**merging them with your container - not overwriting your current container)**, you will need to go through each tag and apply the appropriate category.

## What if I don't use some of the services that are part of the Default Tags?

If you do not use some services for some of the Tags already included then you will need to delete them to delete them so that they do not set cookies for services that you do not actually use on your website: 

![DeleteTag1](https://bitbucket.org/jlfitzsimons/cookie-information-tag-manager-sdk-default-container/raw/2273207f1b38fe2ba6efe7e093f37c2b1286b0be/READMEmedia/Sk%C3%A6rmbillede%202021-06-03%20kl.%2014.59.44.png)


![DeleteTag2](https://bitbucket.org/jlfitzsimons/cookie-information-tag-manager-sdk-default-container/raw/2273207f1b38fe2ba6efe7e093f37c2b1286b0be/READMEmedia/Sk%C3%A6rmbillede%202021-06-03%20kl.%2015.00.06.png)

## What if I need Tags to only fire on particular pages of my website? 

Referring to point 2 above, you will need to adjust the trigger slightly and add some extra conditions that need to be met (as well as consent to a particular cookie category). On left hand menu, click the Tags option.

![Tagmenu](https://bitbucket.org/jlfitzsimons/cookie-information-tag-manager-sdk-default-container/raw/2273207f1b38fe2ba6efe7e093f37c2b1286b0be/READMEmedia/Sk%C3%A6rmbillede%202021-06-03%20kl.%2015.08.04.png)

Choose the tag you would like to adjust and scroll down if need be. Under the heading "Triggering", click on the pencil icon in in the top right corner:

![AdjustingTrigger1](https://bitbucket.org/jlfitzsimons/cookie-information-tag-manager-sdk-default-container/raw/2273207f1b38fe2ba6efe7e093f37c2b1286b0be/READMEmedia/Sk%C3%A6rmbillede%202021-06-03%20kl.%2015.10.28.png)

Hover over the Firing Trigger and click on the > arrow:

![AdjustingTrigger2](https://bitbucket.org/jlfitzsimons/cookie-information-tag-manager-sdk-default-container/raw/2273207f1b38fe2ba6efe7e093f37c2b1286b0be/READMEmedia/Sk%C3%A6rmbillede%202021-06-03%20kl.%2015.13.23.png)

You will then be presented with the Trigger Configuration menu

![TriggerConfigurationMenu](https://bitbucket.org/jlfitzsimons/cookie-information-tag-manager-sdk-default-container/raw/2273207f1b38fe2ba6efe7e093f37c2b1286b0be/READMEmedia/Sk%C3%A6rmbillede%202021-06-03%20kl.%2015.20.23.png)

Click on the + to add a new condition:

![AddingTriggerCondition1](https://bitbucket.org/jlfitzsimons/cookie-information-tag-manager-sdk-default-container/raw/2273207f1b38fe2ba6efe7e093f37c2b1286b0be/READMEmedia/Sk%C3%A6rmbillede%202021-06-03%20kl.%2015.20.47.png)


![AddingTriggerCondition2](https://bitbucket.org/jlfitzsimons/cookie-information-tag-manager-sdk-default-container/raw/2273207f1b38fe2ba6efe7e093f37c2b1286b0be/READMEmedia/Sk%C3%A6rmbillede%202021-06-03%20kl.%2015.22.00.png)